<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Resto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $chef;

    /**
     * @ORM\Column(type="smallint")
     */
    private $nbretoiles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getChef(): ?string
    {
        return $this->chef;
    }

    public function setChef(string $chef): self
    {
        $this->chef = $chef;

        return $this;
    }

    public function getNbrEtoiles(): ?int
    {
        return $this->nbretoiles;
    }

    public function setNbrEtoiles(int $nbrEtoiles): self
    {
        $this->nbretoiles = $nbrEtoiles;

        return $this;
    }

    //Methodes

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function corrigeNomBatiment() {
        $this->chef = strtoupper($this->chef); }
}
