<?php namespace App\Controller;

use App\Entity\Resto;
use App\Form\Type\RestoType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class guideController extends AbstractController
{
    public function accueil()
    {

        return $this->render('guide/accueil.html.twig');
    }
    public function menu()
    {

        return $this->render('guide/menu.html.twig');
    }

    public function find($id)
    {
        $resto = $this->getDoctrine()->getRepository(Resto::class)->find($id);
        if (!$resto)
            throw $this->createNotFoundException('Resto[id=' . $id . '] inexistant');
        return $this->render('guide/voir.html.twig',
            array('resto' => $resto));
    }

    public function findAll(){
        $restos = $this->getDoctrine()->getRepository(Resto::class)->findAll();

        return $this->render('guide/listResto.html.twig',
                                        array('restos' => $restos));
    }

    public function add($nom, $chef, $nbrEtoiles){
        $entityManager = $this->getDoctrine()->getManager();
        $resto = new Resto();
        $resto->setNom($nom);
        $resto->setChef($chef);
        $resto->setNbrEtoiles($nbrEtoiles);

        $entityManager->persist($resto);
        $entityManager->flush();

        return $this->redirectToRoute('find_resto',
                                        array('id' => $resto->getId()));

    }

    public function addForm(Request $request)
    {
        $resto = new Resto;
        $form = $this->createForm(RestoType::class, $resto,
            ['action' => $this->generateUrl('ajout_form')]);
        $form->add('submit', SubmitType::class, array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resto);
            $entityManager->flush();
            return $this->redirectToRoute('find_resto',
                array('id' => $resto->getId()));
        }
            return $this->render('guide/formAdd.html.twig',
                array('form' => $form->createView()));
    }

    public function modifier($id) {
        $resto = $this->getDoctrine()->getRepository(Resto::class)->find($id);
        if(!$resto)
            throw $this->createNotFoundException('Resto[id='.$id.'] inexistante');
        $form = $this->createForm(RestoType::class, $resto,
            ['action' => $this->generateUrl('guide_modifier_suite', array('id' => $resto->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        return $this->render('guide/modifier.html.twig', array('form' => $form->createView()));
    }

    public function modifierSuite(Request $request, $id) {
        $resto = $this->getDoctrine()->getRepository(Resto::class)->find($id);
        if(!$resto)
            throw $this->createNotFoundException('Salle[id='.$id.'] inexistante');
        $form = $this->createForm(RestoType::class, $resto,
            ['action' => $this->generateUrl('guide_modifier_suite', array('id' => $resto->getId()))]);
        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resto);
            $entityManager->flush();
            $url = $this->generateUrl('find_resto',
                array('id' => $resto->getId()));
            return $this->redirect($url);
        }
        return $this->render('guide/modifier.html.twig',
            array('form' => $form->createView()));
    }

    public function findByStar($nbrEtoiles){
        $repo = $this->getDoctrine()->getManager()->getRepository(Resto::class);

        $restos = $repo->findBy(array('nbretoiles'=>$nbrEtoiles));

        return $this->render("guide/listResto.html.twig", array('restos' => $restos));
    }

    public function findByChef($chef){
        $repo = $this->getDoctrine()->getManager()->getRepository(Resto::class);

        $restos = $repo->findBy(array('chef'=>$chef));

        return $this->render("guide/listResto.html.twig", array('restos' => $restos));
    }
}